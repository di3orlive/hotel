jQuery.exists = function (selector) {
    return ($(selector).length > 0);
};


(function() {
    $(document).ready(function() {

        $('.animate-img').viewportChecker({
            classToAdd: 'visible animated fadeIn',
            offset: 150
        });

        $('.animate-text').viewportChecker({
            classToAdd: 'visible animated pulse',
            offset: 150
        });

        $('.text-up').viewportChecker({
            classToAdd: 'visible animated fadeInUp',
            offset: 150
        });

        $('.text-down').viewportChecker({
            classToAdd: 'visible animated fadeInDown',
            offset: 150
        });

        $('.text-flipInY').viewportChecker({
            classToAdd: 'visible animated flipInY',
            offset: 150
        });

        $('.text-zoomIn').viewportChecker({
            classToAdd: 'visible animated zoomIn',
            offset: 150
        });

    });
}).call(this);